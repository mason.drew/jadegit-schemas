# jadegit
This repository is a submodule of the main [jadegit project](https://gitlab.com/jadelab/jadegit/jadegit-schemas), responsible for implementing the schemas used by jadegit.

## License
Like the main project, this repository is licensed under the [GNU Lesser General Public License v3.0+](COPYING.LESSER).
