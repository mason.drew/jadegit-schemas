# Welcome

## Licensing
By contributing to this project, you agree to release your contribution under the terms of the license.  All code is released under the [LGPL v3.0+](COPYING.LESSER).
